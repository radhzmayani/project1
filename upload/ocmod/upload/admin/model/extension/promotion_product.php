<?php

class ModelExtensionPromotionProduct extends Model {

	 public function addPromotionProduct($data) {
		
		$prom_pro_ids = implode(",",$data['prom_product_ids']);
		$free_pro_ids = implode(",",$data['free_product_ids']);
        $this->db->query("INSERT INTO " . DB_PREFIX . "promotion_product SET p_code = '" . $this->db->escape($data['p_code']) . "',prom_product_ids = '" . $prom_pro_ids . "',free_product_ids = '" . $free_pro_ids . "',  status = '" . (int) $data['status'] . "'");

        $id = $this->db->getLastId();

        return $id;
    }
	
	 public function editPromotionProduct($id, $data) {
		
		$prom_pro_ids = implode(",",$data['prom_product_ids']);
		$free_pro_ids = implode(",",$data['free_product_ids']);
        $this->db->query("UPDATE " . DB_PREFIX . "promotion_product SET p_code = '" . $this->db->escape($data['p_code']) . "',prom_product_ids = '" . $prom_pro_ids . "',free_product_ids = '" . $free_pro_ids . "', status = '" . (int) $data['status'] . "' WHERE id = '" . (int) $id . "'");

    }
	
	public function deletePromotionProduct($id) {

        $this->db->query("DELETE FROM " . DB_PREFIX . "promotion_product WHERE id = '" . (int) $id . "'");

    }
	
	 public function getPromotionProduct($id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "promotion_product WHERE id = '" . (int) $id . "'");

        return $query->row;
    }

	public function getTotalPromotionProduct() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "promotion_product");
		
	    return $query->row['total'];
    }
	
	public function getPromotionProducts($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "promotion_product pp";
		
        $implode = array();
	
		if (isset($data['status']) && !is_null($data['status'])) {
			$implode[] = "status = " . (int)$data['status'] ;
		}

		if ($implode) {
			$sql .= " WHERE " . implode("", $implode);
		}

        $sort_data = array(
            'p_code',
			'prom_product_ids',
			'free_product_ids',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY p_code";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);
		
		

        return $query->rows;
    }


    public function createDatabaseTables() {
        $sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "promotion_product` ( ";
        $sql .= "`id` int(11) NOT NULL AUTO_INCREMENT, ";
		$sql .= "`p_code` varchar(96) COLLATE utf8_bin DEFAULT NULL,";
        $sql .= "`prom_product_ids` varchar(96) COLLATE utf8_bin DEFAULT NULL, ";
	    $sql .= "`free_product_ids` varchar(96) COLLATE utf8_bin DEFAULT NULL, ";
	    $sql .= "`status` int(1) NOT NULL DEFAULT '0', ";
        $sql .= "PRIMARY KEY (`id`) ";
        $sql .= ") ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;";
        $this->db->query($sql);
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "module` SET `name` = 'Promotion Product', `code` = 'promotion_product', `setting` = '{\"name\":\"Promotion Product\",\"status\":\"1\"}'");
		
    }

    public function dropDatabaseTables() {
        $sql = "DROP TABLE IF EXISTS `" . DB_PREFIX . "promotion_product`;";
        $this->db->query($sql);
		
		$this->db->query("DELETE FROM `" . DB_PREFIX . "module` WHERE `name` = 'Promotion Product'");

    }

}
