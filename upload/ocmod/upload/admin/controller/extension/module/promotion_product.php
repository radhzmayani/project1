<?php 
class ControllerExtensionModulePromotionProduct extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/promotion_product');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$this->getList();
		
	}
	
	public function insert() {
		$this->load->language('extension/module/promotion_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/promotion_product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_extension_promotion_product->addPromotionProduct($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'], true));
		}

		$this->getForm();
	}
	
	public function update() {
		$this->load->language('extension/module/promotion_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/promotion_product');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_extension_promotion_product->editPromotionProduct($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'], true));
		}

		$this->getForm();
	}
	
	public function delete() {
		$this->load->language('extension/module/promotion_product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/promotion_product');

		if (isset($this->request->post['selected']) && $this->validate()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_extension_promotion_product->deletePromotionProduct($id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'], true));
		}

		$this->getList();
	}
	
	
	protected function getList(){
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'email';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		$data['add'] = $this->url->link('extension/module/promotion_product/insert', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('extension/module/promotion_product/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);	

		$data['promotion_products'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		
		$this->load->model('extension/promotion_product');
		$this->load->model('catalog/product');

		$promotion_products_total = $this->model_extension_promotion_product->getTotalPromotionProduct();

		$results = $this->model_extension_promotion_product->getPromotionProducts($filter_data);
		
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('extension/module/promotion_product/update', 'user_token=' . $this->session->data['user_token'] . '&id=' . $result['id'] . $url, true)
			);
			$prom_pro_name = array();
			$prom_pro_ids = explode(",",$result['prom_product_ids']);
			
			foreach ($prom_pro_ids as $prom_pro_id) {
				$prom_product_info = $this->model_catalog_product->getProduct($prom_pro_id);
	
				if ($prom_product_info) {
					$prom_pro_name[] =  $prom_product_info['name'];
				}
			}
			
			$free_pro_name = array();
			$free_pro_ids = explode(",",$result['free_product_ids']);
			
			foreach ($free_pro_ids as $free_pro_id) {
				$free_product_info = $this->model_catalog_product->getProduct($free_pro_id);
	
				if ($free_product_info) {
					$free_pro_name[] =  $free_product_info['name'];
				}
			}
			
			$data['promotion_products'][] = array(
				'id' 			=> $result['id'],
				'p_code'         => $result['p_code'],
				'prom_product_ids' => implode(",",$prom_pro_name),
				'free_product_ids' => implode(",",$free_pro_name),
 				'status'        => $result['status'],
				'selected'        => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'          => $action
			);
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_p_code'] = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'] . '&sort=p_code' . $url, true);
		$data['sort_prom_product_ids'] = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'] . '&sort=prom_product_ids' . $url, true);
		$data['sort_free_product_ids'] = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'] . '&sort=free_product_ids' . $url, true);
 		$data['sort_status'] = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $promotion_products_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['action'] = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/promotion_product', $data));
	} 
	
	protected function getForm() {
		
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		if (!isset($this->request->get['id'])) {
			$data['action'] = $this->url->link('extension/module/promotion_product/insert', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('extension/module/promotion_product/update', 'user_token=' . $this->session->data['user_token'] . '&id=' . $this->request->get['id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('extension/module/promotion_product', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		
		$this->load->model('extension/promotion_product');
		
		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_promotion_info = $this->model_extension_promotion_product->getPromotionProduct($this->request->get['id']);
		}else{
			$product_promotion_info = '';
		}
		
		$this->load->model('catalog/product');

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($product_promotion_info)) {
			$data['status'] = $product_promotion_info['status'];
		} else {
			$data['status'] = '';
		}
		
		if (isset($this->request->post['p_code'])) {
			$data['p_code'] = $this->request->post['p_code'];
		} elseif (!empty($product_promotion_info)) {
			$data['p_code'] = $product_promotion_info['p_code'];
		} else {
			$data['p_code'] = '';
		}
		
		$data['promoted_products'] = array();
		if($product_promotion_info){
			$prom_products = explode(",",$product_promotion_info['prom_product_ids']);
	
			foreach ($prom_products as $prom_product) {
				$prom_product_info = $this->model_catalog_product->getProduct($prom_product);
	
				if ($prom_product_info) {
					$data['promoted_products'][] = array(
						'product_id' => $prom_product_info['product_id'],
						'name'       => $prom_product_info['name']
					);
				}
			}
		}
		$data['free_products'] = array();
		if($product_promotion_info){
			$free_products = explode(",",$product_promotion_info['free_product_ids']);
	
			foreach ($free_products as $free_product) {
				$free_product_info = $this->model_catalog_product->getProduct($free_product);
	
				if ($free_product_info) {
					$data['free_products'][] = array(
						'product_id' => $free_product_info['product_id'],
						'name'       => $free_product_info['name']
					);
				}
			}
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/promotion_product_form', $data));
		
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/promotion_product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	 public function install() {
        $this->load->model('extension/promotion_product');
        $this->model_extension_promotion_product->createDatabaseTables();
    }

    public function uninstall() {
        $this->load->model('extension/promotion_product');
        $this->model_extension_promotion_product->dropDatabaseTables();
    }
}