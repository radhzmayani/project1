<?php
// Heading
$_['heading_title']    = 'Promotion Product';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Promotion Product module!';
$_['text_edit']        = 'Edit Promotion Product';
$_['text_list']        = 'Promotion Product List';
$_['text_add']           = 'Add Promotion Product';
$_['text_default']       = 'Default';

$_['column_p_code']        = 'Product Code';
$_['column_prom_product_ids']    = 'Promotion Products';
$_['column_free_product_ids']        = 'Free Products';
$_['column_status']        = 'Status';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';

// Entry
$_['entry_status']     = 'Status';
$_['entry_p_code']         = 'Product Code';
$_['entry_prom_product']         = 'Cross Promotion Products'; 
$_['entry_free_product']         = 'Free Products'; 

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Promotion Product module!';